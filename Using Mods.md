## Using Mods

**WARNING**: Mods contain arbitrary code, which will be executed with the same privileges as the game itself. An evil mod may contain malware which can take over your computer, and destroy or steal your data. Do not run mods from untrusted sources.

Mods are distributed as `.zip` files. One `.zip` file contains one mod.

To install a mod, go to the game's installation directory (the directory containing `YomiHustle.exe`) and place the mod `.zip` files in the `mods` directory. If there isnt a `mods` directory, create one. Do not unpack the `.zip` files.

### Online Play

In order to play online with mods, you will need to have every mod your opponent has installed. The modloader will verify that you both have identical loaded mods, and if there is a difference, prevent you from playing. 

This will be updated in the future to only require the mods in use by both players.
