extends "res://characters/wizard/states/MagicMissile.gd"


func _frame_7():
	var dir = host.obj_local_center(host.opponent)
	dir = fixed.normalized_vec(str(dir.x), str(dir.y))
	var obj = host.spawn_object(preload("res://characters/wizard/projectiles/MagicMissile.tscn"), PROJECTILE_X, PROJECTILE_Y, true, {"dir":dir})

func _frame_0():
	var dir = host.obj_local_center(host.opponent)
	dir = fixed.normalized_vec(str(dir.x), str(dir.y))
	var obj = host.spawn_object(preload("res://characters/wizard/projectiles/MagicMissile.tscn"), PROJECTILE_X + 10, PROJECTILE_Y - 35, true, {"dir":dir})